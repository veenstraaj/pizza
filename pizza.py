#!/usr/bin/env python3
# coding=utf8
import json
import os
import sys
import unicodedata
from datetime import datetime
from operator import itemgetter
from urllib import request

from lxml import html

# !pizza geschreven voor SnotBot in #snt door Jelco
# Versie 1.0 - 26 maart 2012

SMALL = "small"
ITALIAN = "italian"
MEDIUM = ""
MENU_FILENAME = "menu.json"
ORDERS_FILENAME = "orders.json"


def try_decode(string):
    if isinstance(string, str):
        return string
    try:
        return string.decode("utf8")
    except UnicodeError:
        return string.decode("latin1")


def get_username():
    return os.environ["MULTI_REALUSER"]


def is_multicast():
    return os.environ.get("MULTI_IS_MULTICAST", True)


def command(catch_all=False):
    def wrap(fun):
        fun.command = True
        fun.catch_all = catch_all
        return fun

    return wrap


def main():
    try:
        args = sys.argv[1]
    except IndexError:
        args = ""
    PizzaBot(args).handle_command()


class PizzaBot:
    order_dict = {}
    mtime = 0
    last_modified_date = None
    pizzas = {}
    size = ""
    closed = False

    def __init__(self, args):
        try:
            if os.path.isfile(MENU_FILENAME):
                with open(MENU_FILENAME, "r") as file:
                    self.pizzas = json.load(file)
            else:
                print("Geen menu ingeladen")
        except json.JSONDecodeError:
            print("Fout in menu bestand, dus misschien opnieuw inladen...")

        try:
            if os.path.isfile(ORDERS_FILENAME):
                with open(ORDERS_FILENAME, "r") as file:
                    data = json.load(file)
                    self.order_dict = data.get("orders", {})
                    self.last_modified_date = datetime.fromtimestamp(data.get("mtime", 0))
                    self.mtime = data.get("mtime", 0)
                    self.closed = data.get("closed", False)
            else:
                print("Geen orders bestand")
        except json.JSONDecodeError:
            print("Fout in orders bestand, dus helaas zijn alle orders verloren gegaan")

        self.args = try_decode(args).strip()

    def save_orders(self, updated):
        if updated and self.closed:
            print("Actie geannuleerd omdat de bestelling al gedaan is door {}. Doe clear of cancel om toch te bestellen.".format(self.closed))
            return False

        with open(ORDERS_FILENAME, "w") as file:
            json.dump({
                "mtime": datetime.now().timestamp() if updated else self.mtime,
                "orders": self.order_dict,
                "closed": self.closed,
            }, file)
        return True

    def findpizza(self, searchstring):
        names = {name.lower(): name for name in self.pizzas.keys()}
        searchstring = searchstring.lower().strip()
        try:
            return [names[searchstring]]
        except KeyError:
            return list(name for name_lower, name in names.items() if searchstring in name_lower)

    def get_modified_date(self):
        return self.last_modified_date.strftime("%d %b %Y") if self.last_modified_date else "Unknown"

    @command()
    def clear(self):
        self.order_dict = {}
        self.closed = False
        self.save_orders(True)
        print("Alle orders verwijderd")

    @command()
    def update(self):
        try:
            menu = html.parse(request.urlopen("https://www.newyorkpizza.nl/bestellen/pizza"))
            product_ids = set(map(int, menu.xpath("//*[contains(@class, 'nyp-product-controls')]//@data-product-id")))
            double_tasty = html.parse(request.urlopen("https://www.newyorkpizza.nl/Menu/_DoubleTastyPizzaModalPartial"))
            double_tasty_ids = set(map(int, double_tasty.xpath("//div[@id='pizzas-firsthalf']//@data-product-id")))

            pizzas = {}
            for pid in product_ids:
                pizza = html.parse(request.urlopen(
                    "https://www.newyorkpizza.nl/Menu/_ProductDetailsModalPartial", b"productId=%d" % pid))
                title = pizza.xpath("string(//h1)").strip()
                desc = pizza.xpath("string(//*[contains(@class, 'nyp-product-seo-text')])").strip()

                # Remove accents from title to make pizzas orderable.
                title = "".join((c for c in unicodedata.normalize("NFKD", title) if not unicodedata.combining(c)))

                pizzas[title] = [pid in double_tasty_ids, desc]
            with open(MENU_FILENAME, "w") as file:
                json.dump(pizzas, file)
            print("Menu ingeladen.")
        except Exception:
            cls, msg, trace = sys.exc_info()
            print("Er ging iets fout op regel {}: {}: {}".format(trace.tb_lineno, cls.__name__, msg))

    @command()
    def menu(self):
        print(", ".join(sorted(self.pizzas.keys())) if self.pizzas else "Nog niet geladen")

    @command(True)
    def info(self):
        pizza = self.findpizza(self.args)
        if not pizza:
            print("Die pizza ken ik niet.")
        elif len(pizza) > 1:
            print("Welke bedoel je? {}?".format(" of ".join(pizza)))
        else:
            can_dt, desc = self.pizzas[pizza[0]]
            print("{}: {}{}".format(pizza[0], desc, " (*niet* als double tasty te bestellen)" if not can_dt else ""))

    @command()
    def orders(self):
        if not self.order_dict:
            print("Nog geen bestellingen geplaatst.")
        else:
            order_set = set(self.order_dict.values())
            order_list = list(self.order_dict.values())
            print("{} [last modified: {}]".format(", ".join(["{}x {}".format(
                order_list.count(order), order,
            ) for order in sorted(order_set)]), self.get_modified_date()))
            self.closed = self.closed or get_username()
            self.save_orders(False)
            print("Orders zijn niet mogelijk tot een cancel of een clear")

    @command()
    def cancel(self):
        if self.closed:
            print("{} heeft blijkbaar niet besteld? Bestellingen zijn weer mogelijk.".format(self.closed))
        else:
            print("Je mag nog gewoon bestellen hoor...")
        self.closed = False
        self.save_orders(False)

    @command()
    def null(self):
        self.remove(get_username())

    def remove(self, username):
        try:
            del self.order_dict[username]
            if self.save_orders(True):
                print("Order verwijderd.")
        except KeyError:
            print("Wie ben jij?")

    @command(True)
    def other(self):
        words = self.args.split(maxsplit=1)
        if len(words) == 2:
            if words[1] == "null":
                self.remove(words[0])
            else:
                self.args = words[1]
                self.order_pizza(words[0])
        else:
            print("Wat?")

    def order_pizza_double(self, username):
        if self.args.count("+") > 1:
            print("Zoveel smaken kan ik niet combineren.")
            return

        if self.args[-1] == "+" or self.args[0] == "+":
            print("Ik mis een smaak.")
            return

        pizzas = list(map(self.findpizza, self.args.split("+")))

        if all(len(matches) == 1 for matches in pizzas):
            matches = list(map(itemgetter(0), pizzas))
            if matches[0] == matches[1]:
                print("Weet je zeker dat je niet gewoon een normale {} wilt bestellen?".format(matches[0]))
                return

            for match in matches:
                if not self.pizzas[match][0]:
                    print("{} kan niet als Double Tasty besteld worden :(".format(match))
                    return

            order = "{}Double Tasty ({} + {})".format(self.size, *sorted(matches))
            self.order_dict[username] = order
            if self.save_orders(True):
                print("Bestelling voor {} geplaatst!".format(order))
        else:
            result = []
            for index, match in enumerate(pizzas, start=1):
                if not match:
                    result.append("Smaak {} ken ik niet.".format(index))
                elif len(match) > 1:
                    result.append("Smaak {} kan {} zijn.".format(index, " of ".join(match)))
            print(" ".join(result))

    def order_pizza(self, username):
        if self.args.startswith(SMALL + " "):
            self.size = "SMALL "
            self.args = self.args[len(SMALL) + 1:].strip()

        if self.args.startswith(ITALIAN + " "):
            self.size += "ITALIAN "
            self.args = self.args[len(ITALIAN) + 1:].strip()

        if username in self.order_dict:
            print("You already ordered, but we will ignore that for now.")

        if "+" in self.args:
            self.order_pizza_double(username)
        else:
            matches = self.findpizza(self.args)
            if len(matches) == 1:
                order = "{}{}".format(self.size, matches[0])
                self.order_dict[username] = order
                if self.save_orders(True):
                    print("Bestelling voor {} geplaatst!".format(order))
            elif not matches:
                print("Die ken ik niet.")
            else:
                print("Welke bedoel je? {}?".format(" of ".join(matches)))

    def who(self):
        if not is_multicast():
            print("Je order: {}".format(self.order_dict.get(get_username(), "niks")))
        print("{} [count: {}; last modified: {}]".format(
            ", ".join(sorted(self.order_dict.keys())) if self.order_dict else "-",
            len(self.order_dict.keys()), self.get_modified_date(),
        ))

    def handle_command(self):
        words = self.args.split(maxsplit=1)
        if words:
            method = getattr(self, words[0], None)
            if getattr(method, "command", False) and (getattr(method, "catch_all")) != (len(words) == 1):
                self.args = words[1] if len(words) > 1 else ""
                method()
            else:
                self.order_pizza(get_username())
        else:
            self.who()


if __name__ == "__main__":
    main()
